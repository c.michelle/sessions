//Dependecies and Module
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('dotenv').config();

const userRoutes = require('./routes/user');
const productRoutes = require('./routes/product');

//Environement Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

//Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.qhaqjae.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
	
});

mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));

//Backend Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);

//Server Gateaway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${process.env.PORT || port}`);
	});
};


module.exports = app;
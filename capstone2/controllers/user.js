//Dependencies and Modules
const User = require('../models/User.js');
//const Product = require("../models/Product.js");
const bcrypt = require('bcrypt');

const auth = require('../auth');


//User Registration
module.exports.registerUser =(reqBody)=>{
	console.log(reqBody);
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user,error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
	.catch(err => err);
}

//User Login Authentication
module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email}).then(result => {
		console.log(result);
		if(result ===null){
			return res.send(false)
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				return res.send({access:auth.createAccessToken(result)})
			}else{
				return res.send(false) //incorrect password
			}
		}
	})
	.catch(err => res.send(err))
}



//retrive user details
module.exports.getProfile = (req,res) =>{
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(error => error)
}






//create order
module.exports.order = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.productId);

	if(req.user.isAdmin){
		return res.send("You cannot do this action.")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newOrder = {
			productId: req.body.productId,

		}

		let listOfOrders = user.orders;

		let filteredProductId = [];

		listOfEnrollments.forEach((result) => {
			filteredProductId.push(result.productId);
		});


		return user.save().then(user => true).catch(error => res.send(error));

	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		let enrollee = {
			userId: req.user.id
		}

		product.enrollees.push(enrollee);

		return product.save().then(product => true).catch(error => res.send(error));
	})

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated});
	}

	if(isUserUpdated && isProductUpdated){
		return res.send("You are now ordered a product. Thank you!");
	}
}

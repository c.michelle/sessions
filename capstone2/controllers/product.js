const Product = require("../models/Product.js");

//Create a new Product
module.exports.addProduct =(req,res)=>{
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	return newProduct.save().then((product,error) => {
		if(error){
			return res.send(false);
		}else{
			return  res.send(true);
		}
	})
	.catch(error => res.send(error));
}

//Retrieve All Products
module.exports.getAllProducts = (req, res) =>{
	return Product.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no product in the database");
		}else{
			return res.send(result);
		}
	})
}


//Retrive All Active Products
module.exports.getAllActive = (req, res) =>{
	return Product.find({isActive: true}).then(result =>{
		if (result.length === 0){
			return res.send("There is currently no active products.")
		}else{
			return res.send(result);
		}
	})
}


//Retrieve single product
module.exports.getProduct = (req,res) =>{
	return Product.findById(req.params.productId).then(result => {
/*		if(result === 0){
			return res.send("Cannot find course with provided ID.")
		}else{
			return res.send(result)
		}*/

		if(result === 0){
			return res.send("Cannot find product with provided ID.")
		}else{
			if(result.isActive === false){
				return res.send("The product you are trying to access is not available.")
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct product ID."));
}


//Updating a product (Admin Only)
module.exports.updateProduct = (req, res) =>{
	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) =>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}



//Archive Product
module.exports.archiveProduct = (req, res) =>{
	let updateActiveField = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField).then((product, error) =>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}



//Activate Products
module.exports.activateProduct = (req, res) =>{
	let updateActiveField = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId, updateActiveField).then((product, error) =>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}



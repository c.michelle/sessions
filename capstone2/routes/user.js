//Dependencies and Modules
const express = require("express");
const router = express.Router();
const userController = require('../controllers/user.js');
const auth = require("../auth.js");

//Destructure from auth
const {verify, verifyAdmin} = auth;


//User Register Routes
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});


//User Login Authentication
router.post('/login', userController.loginUser);


//Get User Details
router.get("/details", verify, userController.getProfile);


//Create Order
//router.post("/order", verify, userController.order);





//Export Route System
module.exports = router;
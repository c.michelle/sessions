const express = require("express");
const productController = require("../controllers/product.js");

const auth = require("../auth.js");

//Destruction from auth
const {verify, verifyAdmin} = auth;

const router = express.Router();

//Create a product POST
router.post("/", verify, verifyAdmin, productController.addProduct);


//Retrieve all products
router.get("/all", productController.getAllProducts);


//Retrive All Active Products
router.get("/", productController.getAllActive);


//Retrieve Single Product
router.get("/:productId" , productController.getProduct);


//Updating a product (Admin Only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);


//Archive a product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);


//Activating a course (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);






module.exports = router;
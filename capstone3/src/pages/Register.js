import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	// Syntax: [stateVariable, setterFunction] = useState(assigned state/initial state)
	// Note: setterFunction is to update the state value
	
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	// Checks if values are successfully binded
	console.log(email);
	console.log(password);
	console.log(confirmPassword);

	function registerUser(event){

		// Prevents the default behavior during submission which is page redirection via form submission
		event.preventDefault();

		fetch('https://capstone2-catimbang.onrender.com/users/register',{
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			/* data is the response from the web service */

			// checks the value of response			
			console.log(data);

			// if the received response is true states will be empty and the user will receive a message 'Thank you for registering'
			// else, if not received response is true, the user will receive a message "Please try again later"
			if(data){
				setEmail('');
				setPassword('');
				setConfirmPassword('');

				Swal.fire({
				title: "Register Successful!",
				icon: "success",
				text: "Thank you for registering"
			})
			}
			else{
				Swal.fire({
				title: "Register Error!",
				icon: "error",
				text: "Please try again"
			})
			}
		})

	}

	// The useEffect will run everytime the state changes in any of the field or values listed in [firstName, lastName, email, mobileNo, password, confirmPassword]
	useEffect(() => {

		if((email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){
			// sets the isActive value to true to enable the button
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	},[email, password, confirmPassword]);

	return(

		(user.access !== undefined) ? //null
			<Navigate to="/courses"/>
		:

		<Form onSubmit = {(event) => registerUser(event)}>
		<h1 className="my-5 text-center">Register</h1>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Confirm Password:</Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e =>  {setConfirmPassword(e.target.value)}}/>
			</Form.Group>

			{/* Conditional Rendering:
				
				- if isActive is true - enabled button
				- else, disabled button
			*/}
			{
				isActive? 
				<Button variant="primary" type="submit">Submit</Button>
				:
				<Button variant="primary" type="submit" disabled>Submit</Button>
			}

		</Form>
		)
}
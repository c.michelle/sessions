import { Card, Button } from 'react-bootstrap';
import productsData from '../data/productsData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component recieves information in a form of an object
	//console.log(typeof props);

	const { _id, name, description, price } = productProp;


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
	    </Card.Body>
	</Card>	
	)
}

// Check if the ProductCard component is getting the correct prop types
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
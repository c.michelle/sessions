//Array Mutator Methods

let fruits = ["apple", "orange", "kiwi", "dragon fruit"];

//push();
//add element in the last of an array

console.log("Current Array: ");
console.log(fruits);

let fruitsLength = fruits.push("mango");
console.log("Array after push method:");
console.log(fruitsLength);
console.log(fruits);

//push multiple elements

fruits.push("avocado", "guava");
console.log("Array after push method:");
console.log(fruits);

function addFruit(fruit){
	//push parameter
	fruits.push(fruit);
	console.log(fruits);
}

addFruit("pineapple");


//pop()
//removes element at the end of an array

fruits.pop();
fruits.pop();
console.log(fruits);

function removeFruit(){
	fruits.pop();
	console.log(fruits);
}

removeFruit();

//unshift()
//add element in the beginning of an array

fruits.unshift("lime", "banana");
console.log("Array after unshift method:");
console.log(fruits);

function unshiftFruit(fruit){
	fruits.unshift(fruit);
	console.log(fruits);
}

unshiftFruit("calamansi");

//shift()
// remove in the beginning of an anrray.

fruits.shift();
console.log("Array after shift method");
console.log(fruits);

function shiftFruit(){
	fruits.shift();
	console.log(fruits);
}

shiftFruit();

//splice()
//simulataneously removes elements from a spliced index number and adds elemets
//syntax: array.splice(startIndex, deleteCount,elementsToBeAdded)

fruits.splice(1, 2, "avocado");
console.log("Array after splice method");
console.log(fruits);

function spliceFruit(index, deleteCount,fruit){
	fruits.splice(index,deleteCount,fruit);
	console.log(fruits);
}

spliceFruit(1, 1, "cherry");
spliceFruit(2, 1, "durian");


//sort()
//arranges the elements in alphanumeric order

fruits.sort();
console.log("Array after sort method");
console.log(fruits);


//reverse()

fruits.reverse();
console.log("Array after sort method");
console.log(fruits);

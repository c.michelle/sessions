//console.log("Hello World!");

// [SECTION] Arithmetic Operators

let x = 26;
let y = 5;

let sum = x + y;
console.log("Result from addition operator: " + sum);

let difference = x - y;
console.log("Result from subtraction operator: " + difference);

let product = x * y;
console.log("Result from product operator: " + product);

let quotient = x / y;
console.log("Result from division operator: " + quotient);

//Modulo %
let remainder = x % y;
console.log("Result from modulo operator: " + remainder);

//[SECTION] Assignment Operator
//Basic Assignment Operator (=)

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Addition assignment: " +assignmentNumber);

//Shorthand Method for Assignment Method

assignmentNumber +=2;
console.log("Addition assignment: " +assignmentNumber);

assignmentNumber -=2;
console.log("Subtraction assignment: " +assignmentNumber);

assignmentNumber *=2;
console.log("Multiplication assignment: " +assignmentNumber);

assignmentNumber /=2;
console.log("Division assignment: " +assignmentNumber);

// Multiple Operators and Parentheses
/*
            - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
        */

let mdas = 1+2-3*4/5;
console.log("Result from MDAS operation: " + mdas);

/*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
        */

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result from PEMDAS operation: " + pemdas);

//Increment and Decrement
//Incrementation -> ++
//Decrementation -> --

let z = 1;

//pre-increment
let increment = ++z;
console.log("Pre-increment: " + increment); 
console.log("Pre-increment " + z);

//post-increment
increment = z++;
console.log("Post-increment: " + increment); 

let myNum1 = 1;
let myNum2 = 1;

let preIncrement = ++myNum1;
preIncrement = ++myNum1;
console.log(preIncrement);

let postIncrement = myNum2++;
postIncrement = myNum2++;
console.log(postIncrement);

let myNumA = 5;
let myNumB = 5;

//Pre-decrement
let preDecrement = --myNumA;
preDecrement = --myNumA;
console.log(preDecrement);

//Post-decremate
let postDecrement = myNumB--;
postDecrement = myNumB--;
console.log(postDecrement);

//[SECTION] Type Coerciion
//Automatic or implicit conversion of value from one data type to another

let myNum3 = "10";
let myNum4 = 12;

let coercion = myNum3 + myNum4;
console.log(coercion);
console.log(typeof coercion);

let myNum5 = true + 1;
console.log(myNum5);

let myNum6 = false + 1;
console.log(myNum6);

//[SECTION] Comparison Operator

let juan = "juan";

//Equality Operator (==)
console.log(juan == "juan"); //true
console.log(1 == 2); //false
console.log(false == 0); //true

//Inequality Operator (!=)
//! -> NOT
console.log(juan != "juan"); //false
console.log(1 != 2); //true
console.log(false != 0); //false

//Strict Equality Operator (===)
//Same data type, content or value
console.log(1 === 1); //true
console.log(false === 0); //false

//Strict Inequality (!==)
console.log(1 !== 1); //flase
console.log(false !== 0); //true

//[SECTION] Relational Operator
//Comparison operators whether one value is greater or less than to the other value.

let a = 50;
let b = 65;

let example1 = a > b;
console.log(example1); //false

let example2 = a < b;
console.log(example2); //true

let example3 = a >= b;
console.log(example3); //false

let example4 = a <= b;
console.log(example4); //false

// [SECTION] Logical Operator

//Logical "AND" (&& -> double ampersand)
let isLegalAge = true;
let isRegistered = true;

//All conditions should be true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical AND: " +allRequirementsMet);

//Logical "OR" (|| -> double pipe) 
//

let isLegalAge2 = true;
let isRegistered2 = false;

let someRequirementsMet = isLegalAge2 || isRegistered2;
console.log(someRequirementsMet);

//Logical "NOT" (! -> exclamation point)

 let isRegistered3 = false;
 let someRequirementsNotMet = !isRegistered3;
 console.log("Result from logical NOT: " +someRequirementsNotMet);
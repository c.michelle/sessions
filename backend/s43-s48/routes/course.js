const express = require("express");
const courseController = require("../controllers/course.js");

const auth = require("../auth.js");

//Destructure from auth
const {verify, verifyAdmin} = auth;

const router = express.Router();


//create a course POST (admin only)
router.post("/", verify,verifyAdmin, courseController.addCourse);


//get all courses
router.get("/all", courseController.getAllCourses);


//Get all "Active" Course
router.get("/", courseController.getAllActive);


//Get 1 specific course using its ID
router.get("/:courseId", courseController.getCourse);


//Updating a course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);


//Archive a course (Admin Only)
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

//Activating a course (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, courseController.activateCourse);




module.exports = router;
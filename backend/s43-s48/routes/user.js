// [SECTION] Dependencies and Modules
const express = require("express");
const router = express.Router(); //[SECTION] Routing Component
const userController = require('../controllers/user');
const auth = require("../auth.js");

//Destructure from auth
const {verify, verifyAdmin} = auth;




//check email routes
router.post('/checkEmail', (req,res)=> {
	userController.checkEmailExist(req.body).then(resultFromController=> res.send(resultFromController));
})

//user registration routes
router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//user authentication login
router.post('/login', userController.loginUser);



//retrieve user details
/*router.post("/details", verify, (req,res) =>{
	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
})
*/

router.get("/details", verify, userController.getProfile);


//enroll user to a course
router.post("/enroll", verify, userController.enroll);


//get Enrollments
router.get("/getEnrollments", verify, userController.getEnrollments);

//reset password
router.put("/reset-password", verify, userController.resetPassword);


//[SECTION] Export Route System
module.exports = router;

